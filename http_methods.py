import requests

"""создание HTTP методов"""

class Http_methods():
    headers = {'Content-type': 'Application/json'}
    cookies = ''
    @staticmethod
    def post(url, body):
        result = requests.post(url, json=body, headers=Http_methods.headers, cookies=Http_methods.cookies)
        return result
    
    @staticmethod
    def get(url):
        result = requests.post(url, headers=Http_methods.headers, cookies=Http_methods.cookies)
        return result
    
    @staticmethod
    def put(url, body):
        result = requests.put(url, json=body, headers=Http_methods.headers, cookies=Http_methods.cookies)
        return result
    
    @staticmethod
    def delete(url, body):
        result = requests.delete(url, json=body, headers=Http_methods.headers, cookies=Http_methods.cookies)
        return result
        






















