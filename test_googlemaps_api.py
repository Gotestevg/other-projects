from utils.api import Api_methods

"""Создание, изменение и удаление новой локации"""

class Test_google_maps_api():
    def test_google_maps_api_post(self):
        
        print('\nPOST Method')
        post_result = Api_methods.post_method()
        # вывод json и check id
        check_post = post_result.json()
        place_id = check_post.get("place_id")
        
        print('\nGET POST Method')
        get_result = Api_methods.get_method(place_id)
        
        print('\nPUT Method')
        put_result = Api_methods.put_method(place_id)

        print('\nGET Method')
        get_result = Api_methods.get_method(place_id)

        print('\nDEL Method')
        del_result = Api_methods.del_method(place_id)



















